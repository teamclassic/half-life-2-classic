import tkinter as tk
from tkinter import filedialog
from tkinter import *
import os
from os import listdir
import re

# Information
print("VMFClassicifier - V2.2")
print("This program is made by Team Classic specifically for the mod Half-Life 2: Classic.")

# Open the texture files
print("\nOpening Texture List Files...")
HL1t = open("hl1texturelist.txt","r").read().split("\n")
HL2t = open("hl2texturelist.txt","r").read().split("\n")
badEnts = open("badents.txt","r").read().split("\n")
print("Done")

dirFiles = []
#Ask user to open source dir and scan for VMF files
root = Tk()
root.dir1 = filedialog.askdirectory(initialdir = "/", title = "VMF files location")
for b in listdir(root.dir1):
    if (b.endswith('.vmf')):
        dirFiles.append(b)
#Ask user to open destination dir
root.dir2 = filedialog.askdirectory(initialdir = "/", title = "Select destination")

#Repeat for number of files
for c in range(0, len(dirFiles)):
    #Get Files
    openFile = open(root.dir1 + "\\" + dirFiles[c], 'r+')
    resultFile = open(root.dir2 + "\\" + dirFiles[c], 'a+')
    getFile = openFile.read();
    
    #Get rid of unwanted addons (_cheap, _c17, etc.)
    getFile = getFile.replace("_CHEAP","")
    getFile = getFile.replace("_C17","")
    getFile = getFile.replace("_LOWFRICT","")
    getFile = getFile.replace("_NOPASSBULLETS","")
    getFile = getFile.replace("_ANTLION","")
    getFile = getFile.replace("METAL/CITADEL_","METAL/")
    #Replace certain ents
    getFile = getFile.replace("npc_maker","monstermaker")
    getFile = getFile.replace("npc_metropolice","monster_metrocop")
    getFile = getFile.replace("npc_","monster_")
    getFile = getFile.replace("func_breakable_surf","func_breakable")
    getFile = getFile.replace("func_brush","func_detail")
    getFile = getFile.replace("func_lod","func_detail")
    getFile = getFile.replace("info_node_hint","info_node")
    getFile = getFile.replace("info_node_air_hint","info_node_air")
    #Replace texture names
    for x in range(0, len(HL2t)):
         getFile = getFile.replace(HL2t[x], HL1t[x])

    #Split file by line (prevents crashing)
    wholeFile = getFile.split("\n")
    
    deleteMode = 0
    for a in range(0, len(wholeFile)):
        getFile = wholeFile[a]    
        getFile = getFile.replace("\t", " @TaB ")
        file = getFile.split(" ")
        
        #Remove unwanted entities
        if(wholeFile[a] == "entity" or wholeFile[a] == "hidden"):
            for h in range (0, len(badEnts)):
                if(wholeFile[a+3] == '\t"classname" "' + badEnts[h] + '"' or wholeFile[a+3] == '\t"classname" "' + badEnts[h] + '" '):
                    deleteMode = 1
                    break
                elif(wholeFile[a+5] == '\t\t"classname" "' + badEnts[h] + '"' or wholeFile[a+5] == '\t\t"classname" "' + badEnts[h] + '" '):
                    deleteMode = 1
                    break
        if (deleteMode == 1):
            if(file[0] == "}"):
                deleteMode = 0
        else:
            #Texture scaling
            for y in range (0,len(file)):
                if(file[y] == "\"uaxis\"" or file[y] == "\"vaxis\""):
                    file2 = file[y+5]
                    file3 = ''.join(file2)
                    file4 = file3[:-1]
                    file[y+5] = str(float(file4)*4) + '\"'
                    break
        
            #Glue everything together and add back whitespace
            result = file[0]
            for z in range(1, len(file)):
                if (file[z] == "@TaB"):
                    result += "\t"
                else:
                    result += file[z] + " "
            resultFile.write(result + "\n")
    print("Finished " + dirFiles[c])

openFile.close()
resultFile.close()
print("done")
