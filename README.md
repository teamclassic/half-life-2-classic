Half-Life 2 Classic is a Half-Life 1 mod that aims to recreate the entirety of Half-Life 2.

Why? Mainly, just for fun and because a lot of people like how the goldsrc engine feels, or enjoy the gameplay or Half-Life more than Half-Life 2.
It's also a way to see what Half-Life 2 could have looked like in the limits of Goldsrc, if valve never developed the Source engine.